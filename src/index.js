import React from 'react';
import ReactDOM from 'react-dom';

const wasm = import("../build/react_rust_wasm");

const do_the_thing = wasm => {
  wasm.hello("World!")
};

wasm.then(wasm => {

  const App = () => {
    return (
      <div>
        <h1>Hi there</h1>
        <button onClick={do_the_thing.bind(null, wasm)}>Run Computation</button>
      </div>
    );
  }

  ReactDOM.render(
    <App />,
    document.getElementById('root')
  );
});
