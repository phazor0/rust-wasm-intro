extern crate wasm_bindgen;

use wasm_bindgen::prelude::*;

#[wasm_bindgen]
extern {
    fn alert(s: &str);
}

#[wasm_bindgen]
pub fn hello(s: &str) {
    let value = &do_calculation().to_string();
    let arr = ["Hello", s, value];
    let long_string = &arr.join(" ");
    alert(long_string);
}

fn do_calculation() -> i32 {
    println!("entering calculation");
    return 5;
}
